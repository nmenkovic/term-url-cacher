package com.nmenkovic.assignment.termurlcacher.queue.term;

import com.nmenkovic.assignment.termurlcacher.queue.QueueProducer;

public class TermsQueueProducer implements QueueProducer<String> {

    @Override
    public void addElement(String element) {
        TermsQueue.getInstance().getQueue().add(element);
    }
}
