package com.nmenkovic.assignment.termurlcacher.queue;

public interface QueueConsumer<T> {

    T poll();
}
