package com.nmenkovic.assignment.termurlcacher.queue.term;

import com.nmenkovic.assignment.termurlcacher.queue.QueueConsumer;

public class TermsQueueConsumer implements QueueConsumer<String>{

    @Override
    public String poll() {
        return TermsQueue.getInstance().getQueue().poll();
    }
}
