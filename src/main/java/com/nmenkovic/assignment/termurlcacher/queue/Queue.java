package com.nmenkovic.assignment.termurlcacher.queue;

import java.util.concurrent.ConcurrentLinkedQueue;

public abstract class Queue<T> {

    protected ConcurrentLinkedQueue<T> queue = new ConcurrentLinkedQueue<>();

    public ConcurrentLinkedQueue<T> getQueue() {
        return queue;
    }

}
