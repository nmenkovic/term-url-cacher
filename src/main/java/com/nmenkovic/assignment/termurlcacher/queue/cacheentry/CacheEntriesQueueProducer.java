package com.nmenkovic.assignment.termurlcacher.queue.cacheentry;

import com.nmenkovic.assignment.termurlcacher.model.TermCacheEntry;
import com.nmenkovic.assignment.termurlcacher.queue.QueueProducer;

public class CacheEntriesQueueProducer implements QueueProducer<TermCacheEntry>{

    @Override
    public void addElement(TermCacheEntry element) {
        CacheEntriesQueue.getInstance().getQueue().add(element);
    }
}
