package com.nmenkovic.assignment.termurlcacher.queue;

public interface QueueProducer<T> {

    void addElement(T element);

}
