package com.nmenkovic.assignment.termurlcacher.queue.term;

import com.nmenkovic.assignment.termurlcacher.queue.Queue;

public class TermsQueue extends Queue<String> {

    private static TermsQueue instance = null;
    private static Object mutex = new Object();

    private volatile boolean fillingQueueFinished = false;

    private TermsQueue() {
    }

    public static TermsQueue getInstance() {
        if (instance == null) {
            synchronized (mutex) {
                if (instance == null) {
                    instance = new TermsQueue();
                }
            }
        }
        return instance;
    }

    public void setFillingQueueFinished(boolean fillingQueueFinished) {
        this.fillingQueueFinished = fillingQueueFinished;
    }

    public boolean isFillingQueueFinished() {
        return fillingQueueFinished;
    }
}
