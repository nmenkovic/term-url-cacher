package com.nmenkovic.assignment.termurlcacher.queue.cacheentry;

import com.nmenkovic.assignment.termurlcacher.model.TermCacheEntry;
import com.nmenkovic.assignment.termurlcacher.queue.QueueConsumer;

public class CacheEntriesQueueConsumer implements QueueConsumer<TermCacheEntry> {

    @Override
    public TermCacheEntry poll() {
        return CacheEntriesQueue.getInstance().getQueue().poll();
    }
}
