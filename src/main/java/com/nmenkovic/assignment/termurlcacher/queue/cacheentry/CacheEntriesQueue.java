package com.nmenkovic.assignment.termurlcacher.queue.cacheentry;

import com.nmenkovic.assignment.termurlcacher.model.TermCacheEntry;
import com.nmenkovic.assignment.termurlcacher.queue.Queue;

public class CacheEntriesQueue extends Queue<TermCacheEntry>{

    private static CacheEntriesQueue instance = null;
    private static Object mutex = new Object();

    private CacheEntriesQueue() {
    }

    public static CacheEntriesQueue getInstance() {
        if (instance == null) {
            synchronized (mutex) {
                if (instance == null) {
                    instance = new CacheEntriesQueue();
                }
            }
        }
        return instance;
    }

}
