package com.nmenkovic.assignment.termurlcacher.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.MessageDigest;

public class MD5AlgorithmUtil {

    private static final Logger LOGGER = LogManager.getLogger(MD5AlgorithmUtil.class);

    private MD5AlgorithmUtil() {
    }

    public static String hash(String input) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            byte[] array = messageDigest.digest(input.getBytes());

            StringBuilder builder = new StringBuilder();
            for (byte b : array) {
                builder.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, Constants.HEXADECIMAL_HASH_EXPONENT));
            }
            return builder.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            LOGGER.warn(e);
        }
        return null;
    }
}
