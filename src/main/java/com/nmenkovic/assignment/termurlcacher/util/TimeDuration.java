package com.nmenkovic.assignment.termurlcacher.util;

public class TimeDuration {

    private TimeDuration() {
    }

    public static long inMillis(long startTime, long endTime) {
        return endTime - startTime;
    }

}
