package com.nmenkovic.assignment.termurlcacher.util;

public class Constants {

    public static final int MAX_PARALLEL_QUERY_THREADS = 3;
    public static final int NUMBER_OF_CACHE_FILES = 10;
    public static final int HEXADECIMAL_HASH_EXPONENT = 2;

    public static final String CACHE_DIR = "cache";
    public static final String CACHE_FILE_PREFIX = "cache";
    public static final String CACHE_FILE_EXTENSION = "txt";

    public static final String TERMS_FILENAME = "terms.txt";

    public static final String BASE_URL = "http://www.nature.com/";
    public static final String QUERY_PATH = "opensearch/request?httpAccept=application/json&query=";

    public static final String FEED_ELEMENT_NAME = "feed";
    public static final String ENTRY_ELEMENT_NAME = "entry";
    public static final String LINK_ELEMENT_NAME = "link";

    private Constants() {
    }
}
