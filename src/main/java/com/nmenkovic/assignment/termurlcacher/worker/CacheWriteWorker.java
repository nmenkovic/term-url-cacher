package com.nmenkovic.assignment.termurlcacher.worker;

import com.nmenkovic.assignment.termurlcacher.model.TermCacheEntry;
import com.nmenkovic.assignment.termurlcacher.queue.QueueConsumer;
import com.nmenkovic.assignment.termurlcacher.queue.cacheentry.CacheEntriesQueueConsumer;
import com.nmenkovic.assignment.termurlcacher.writer.CacheWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CacheWriteWorker implements Runnable {

    private volatile static boolean finished = false;

    private QueueConsumer queueConsumer = new CacheEntriesQueueConsumer();
    private CacheWriter cacheWriter = new CacheWriter();

    public static void stopRunning() {
        finished = true;
    }

    @Override
    public void run() {
        TermCacheEntry entry;
        while (true) {
            entry = (TermCacheEntry) queueConsumer.poll();
            if (entry == null && finished) {
                break;
            } else if (entry != null) {
                cacheWriter.saveToCacheFile(entry);
            }
        }
    }

}
