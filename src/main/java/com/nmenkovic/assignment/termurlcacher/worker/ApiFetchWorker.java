package com.nmenkovic.assignment.termurlcacher.worker;

import com.nmenkovic.assignment.termurlcacher.model.TermCacheEntry;
import com.nmenkovic.assignment.termurlcacher.provider.NatureOpenApiProvider;
import com.nmenkovic.assignment.termurlcacher.queue.QueueConsumer;
import com.nmenkovic.assignment.termurlcacher.queue.QueueProducer;
import com.nmenkovic.assignment.termurlcacher.queue.cacheentry.CacheEntriesQueueProducer;
import com.nmenkovic.assignment.termurlcacher.queue.term.TermsQueue;
import com.nmenkovic.assignment.termurlcacher.queue.term.TermsQueueConsumer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.CountDownLatch;

public class ApiFetchWorker implements Runnable {

    private static final Logger LOGGER = LogManager.getLogger(ApiFetchWorker.class);

    private QueueConsumer termsQueueConsumer = new TermsQueueConsumer();
    private QueueProducer cacheEntriesQueueProducer = new CacheEntriesQueueProducer();

    private CountDownLatch countDownLatch;
    private NatureOpenApiProvider natureOpenApiProvider;

    public ApiFetchWorker(CountDownLatch countDownLatch, NatureOpenApiProvider natureOpenApiProvider) {
        this.countDownLatch = countDownLatch;
        this.natureOpenApiProvider = natureOpenApiProvider;
    }

    @Override
    public void run() {
        LOGGER.debug("Started thread");
        while (true) {
            String term = (String) termsQueueConsumer.poll();
            if (term == null && TermsQueue.getInstance().isFillingQueueFinished()) {
                break;
            } else if (term != null) {
                TermCacheEntry entry = natureOpenApiProvider.fetch(term);
                cacheEntriesQueueProducer.addElement(entry);
                LOGGER.info("Fetched " + entry);
            }
        }
        LOGGER.debug("Exiting the thread");
        countDownLatch.countDown();
    }
}



