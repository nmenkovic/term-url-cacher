package com.nmenkovic.assignment.termurlcacher.helper;

import com.google.gson.stream.JsonReader;
import com.nmenkovic.assignment.termurlcacher.model.TermCacheEntry;
import com.nmenkovic.assignment.termurlcacher.util.Constants;

import java.io.IOException;

public class TermJsonTranslator {

    public static TermCacheEntry translate(JsonReader reader, String term) throws IOException {
        TermCacheEntry entry = new TermCacheEntry(term);
        entry.setLink(readObject(reader, false));
        return entry;
    }

    private static String readObject(JsonReader reader, boolean objectInEntryArray) throws IOException {
        String result = null;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();

            switch (name) {
                case Constants.FEED_ELEMENT_NAME:
                    result = readObject(reader, false);
                    break;
                case Constants.ENTRY_ELEMENT_NAME:
                    result = readEntryItems(reader);
                    break;
                case Constants.LINK_ELEMENT_NAME:
                    if (objectInEntryArray) {
                        result = reader.nextString();
                        break;
                    }
                default:
                    reader.skipValue();
            }
        }
        reader.endObject();
        return result;
    }

    private static String readEntryItems(JsonReader reader) throws IOException {
        String result = null;
        reader.beginArray();
        int iteration = 0;
        while (reader.hasNext()) {
            if (iteration == 0) {
                result = readObject(reader, true);
            } else {
                reader.skipValue();
            }
            iteration++;
        }
        reader.endArray();
        return result;
    }

}
