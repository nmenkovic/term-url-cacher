package com.nmenkovic.assignment.termurlcacher.helper;

import com.nmenkovic.assignment.termurlcacher.util.TimeDuration;

import java.util.IntSummaryStatistics;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Statistics {

    private static Statistics instance = null;
    private static Object mutex = new Object();

    private ConcurrentLinkedQueue<Integer> requestsDuration = new ConcurrentLinkedQueue<>();
    private int fileReadingDuration;

    private Statistics() {
    }

    public static Statistics getInstance() {
        if (instance == null) {
            synchronized (mutex) {
                if (instance == null) {
                    instance = new Statistics();
                }
            }
        }
        return instance;
    }

    public int getFileReadingDuration() {
        return fileReadingDuration;
    }

    public void setFileReadingDuration(long startTime, long endTime) {
        this.fileReadingDuration = (int) TimeDuration.inMillis(startTime, endTime);
    }

    public void addRequestDuration(long startTime, long endTime) {
        this.requestsDuration.add((int) TimeDuration.inMillis(startTime, endTime));
    }

    public IntSummaryStatistics getSummaryRequestDurationStatistics() {
        return requestsDuration
                .stream()
                .mapToInt(Integer::intValue)
                .summaryStatistics();
    }

}
