package com.nmenkovic.assignment.termurlcacher;

import com.nmenkovic.assignment.termurlcacher.controller.ApplicationController;
import com.nmenkovic.assignment.termurlcacher.worker.CacheWriteWorker;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {

    private static final Logger LOGGER = LogManager.getLogger(Application.class);

    public static void main(String args[]) {
        LOGGER.info("Starting Application execution");

        CacheWriteWorker worker = new CacheWriteWorker();
        ApplicationController controller = new ApplicationController(worker);

        try {
            controller.execute();
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

}
