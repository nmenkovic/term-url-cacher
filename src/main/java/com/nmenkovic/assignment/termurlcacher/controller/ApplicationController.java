package com.nmenkovic.assignment.termurlcacher.controller;

import com.nmenkovic.assignment.termurlcacher.helper.Statistics;
import com.nmenkovic.assignment.termurlcacher.provider.NatureOpenApiProvider;
import com.nmenkovic.assignment.termurlcacher.queue.term.TermsQueue;
import com.nmenkovic.assignment.termurlcacher.queue.term.TermsQueueProducer;
import com.nmenkovic.assignment.termurlcacher.util.Constants;
import com.nmenkovic.assignment.termurlcacher.worker.ApiFetchWorker;
import com.nmenkovic.assignment.termurlcacher.worker.CacheWriteWorker;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.IntSummaryStatistics;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Stream;

public class ApplicationController {

    private static final Logger LOGGER = LogManager.getLogger(ApplicationController.class);

    private final CacheWriteWorker cacheWriteWorker;
    private TermsQueueProducer termsQueueProducer = new TermsQueueProducer();

    public ApplicationController(CacheWriteWorker cacheWriteWorker) {
        this.cacheWriteWorker = cacheWriteWorker;
    }

    public void execute() throws IOException {
        CountDownLatch latch = new CountDownLatch(Constants.MAX_PARALLEL_QUERY_THREADS);

        startApiFetchWorkers(latch);
        startCacheWriterWorker();
        fillTermsQueue();

        LOGGER.info(String.format("Finished reading files in %d ms", Statistics.getInstance().getFileReadingDuration()));

        waitForApiFetchWorkersToFinish(latch);
        stopCacheWriterWorker();

        IntSummaryStatistics summaryStatistics = Statistics.getInstance().getSummaryRequestDurationStatistics();
        LOGGER.info(String.format("Total search request duration %d ms, average duration was %.2f ms, max duration %d ms, min duration %d",
                summaryStatistics.getSum(), summaryStatistics.getAverage(), summaryStatistics.getMax(), summaryStatistics.getMin()));
    }

    private void fillTermsQueue() throws IOException {
        Path path = Paths.get(Constants.TERMS_FILENAME);
        long readingStartTime = Instant.now().toEpochMilli();

        try (Stream<String> lines = Files.lines(path)) {
            lines.forEach(line -> termsQueueProducer.addElement(line));
        } catch (IOException e) {
            LOGGER.error(e);
            throw e;
        }

        long readingEndTime = Instant.now().toEpochMilli();
        TermsQueue.getInstance().setFillingQueueFinished(true);
        Statistics.getInstance().setFileReadingDuration(readingStartTime, readingEndTime);
    }

    private void startCacheWriterWorker() {
        (new Thread(cacheWriteWorker)).start();
    }

    private void stopCacheWriterWorker() {
        cacheWriteWorker.stopRunning();
    }

    private void startApiFetchWorkers(CountDownLatch latch) {
        for (int i = 0; i < Constants.MAX_PARALLEL_QUERY_THREADS; i++) {
            Thread thread = new Thread(new ApiFetchWorker(latch, new NatureOpenApiProvider(new DefaultHttpClient())));
            thread.start();
        }
    }

    private void waitForApiFetchWorkersToFinish(CountDownLatch latch){
        try {
            latch.await();
        } catch (InterruptedException e) {
            LOGGER.error(e);
        }
    }

}
