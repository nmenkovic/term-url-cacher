package com.nmenkovic.assignment.termurlcacher.writer;

import com.google.gson.Gson;
import com.nmenkovic.assignment.termurlcacher.model.TermCacheEntry;
import com.nmenkovic.assignment.termurlcacher.util.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class CacheWriter {

    private static final Logger LOGGER = LogManager.getLogger(CacheWriter.class);

    private int[] cacheIndexOffset = null;

    public void saveToCacheFile(TermCacheEntry entry) {
        if (cacheIndexOffset == null) {
            initializeIndexOffset();
        }
        int hash = entry.hashCode();
        int fileNumber = determineCacheFile(hash);
        createCacheDir();
        Path file = Paths.get(Constants.CACHE_DIR + File.separator + Constants.CACHE_FILE_PREFIX + fileNumber + "." +
                Constants.CACHE_FILE_EXTENSION);
        Gson gson = new Gson();
        String line = gson.toJson(entry) + System.lineSeparator();
        try {
            Files.write(file, line.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            LOGGER.error(e);
        }
    }

    private void createCacheDir() {
        File file = new File(Constants.CACHE_DIR);
        if (!file.exists()) {
            if (file.mkdir()) {
                LOGGER.info("Directory " + Constants.CACHE_DIR + " successfully created.");
            } else {
                LOGGER.error("Failed to create directory!");
            }
        }
    }

    private int determineCacheFile(int hash) {
        for (int i = 0; i < cacheIndexOffset.length - 1; i++) {
            if (i == 0 && cacheIndexOffset[i] > hash) {
                return i;
            } else if (cacheIndexOffset[i] <= hash && cacheIndexOffset[i + 1] > hash) {
                return i + 1;
            }
        }
        return -1;
    }

    private void initializeIndexOffset() {
        cacheIndexOffset = new int[Constants.NUMBER_OF_CACHE_FILES];
        double increment = (Math.pow(16, Constants.HEXADECIMAL_HASH_EXPONENT)) / Constants.NUMBER_OF_CACHE_FILES;
        for (int i = 0; i < Constants.NUMBER_OF_CACHE_FILES; i++) {
            LOGGER.debug("Upper bound for " + i + " is " + ((i + 1) * increment));
            cacheIndexOffset[i] = (int) ((i + 1) * increment);
        }
    }

}
