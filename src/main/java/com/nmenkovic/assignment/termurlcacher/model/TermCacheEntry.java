package com.nmenkovic.assignment.termurlcacher.model;

import com.nmenkovic.assignment.termurlcacher.util.MD5AlgorithmUtil;

public class TermCacheEntry {

    private final String term;
    private String link;
    private long timestamp;
    private long searchReqDuration;
    private transient int hash = -1;

    public TermCacheEntry(String term) {
        this.term = term;
    }

    public long getSearchReqDuration() {
        return searchReqDuration;
    }

    public void setSearchReqDuration(long searchReqDuration) {
        this.searchReqDuration = searchReqDuration;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTerm() {
        return term;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public int hashCode() {
        if (hash == -1) {
            String md5 = MD5AlgorithmUtil.hash(term);
            if (md5 != null && md5.length() >= 2) {
                hash = Integer.parseInt(md5.substring(0, 2).trim(), 16);
            }
        }
        return hash;
    }

    @Override
    public String toString() {
        return term + " " + link + " " + timestamp + " " + searchReqDuration;
    }
}
