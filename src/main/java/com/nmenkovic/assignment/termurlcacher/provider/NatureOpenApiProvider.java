package com.nmenkovic.assignment.termurlcacher.provider;

import com.google.gson.stream.JsonReader;
import com.nmenkovic.assignment.termurlcacher.helper.Statistics;
import com.nmenkovic.assignment.termurlcacher.helper.TermJsonTranslator;
import com.nmenkovic.assignment.termurlcacher.model.TermCacheEntry;
import com.nmenkovic.assignment.termurlcacher.util.Constants;
import com.nmenkovic.assignment.termurlcacher.util.TimeDuration;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.time.Instant;

public class NatureOpenApiProvider {

    private static final Logger LOGGER = LogManager.getLogger(NatureOpenApiProvider.class);

    private HttpClient httpClient;

    public NatureOpenApiProvider(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    public TermCacheEntry fetch(String term) {
        HttpGet request = new HttpGet(Constants.BASE_URL + Constants.QUERY_PATH + term);
        long startTime = Instant.now().toEpochMilli();
        InputStream content = null;
        try {
            HttpResponse response = httpClient.execute(request);
            content = response.getEntity().getContent();
        } catch (IOException e) {
            LOGGER.error(e);
        }
        long endTime = Instant.now().toEpochMilli();
        TermCacheEntry entry = resolveTermCacheEntry(term, content, startTime, endTime);
        Statistics.getInstance().addRequestDuration(startTime, endTime);

        return entry;
    }

    private TermCacheEntry resolveTermCacheEntry(String term, InputStream responseEntityContent, long startTime, long endTime) {
        Reader streamReader = new InputStreamReader(responseEntityContent);
        JsonReader reader = new JsonReader(streamReader);

        TermCacheEntry entry = null;
        try {
            entry = TermJsonTranslator.translate(reader, term);
            entry.setTimestamp(startTime);
            entry.setSearchReqDuration(TimeDuration.inMillis(startTime, endTime));
        } catch (IOException e) {
            LOGGER.error(e);
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                LOGGER.error(e);
            }
        }
        return entry;
    }

}
