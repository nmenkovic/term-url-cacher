package com.nmenkovic.assignment.termurlcacher.helper;

import com.google.gson.stream.JsonReader;
import com.nmenkovic.assignment.termurlcacher.model.TermCacheEntry;
import com.nmenkovic.assignment.termurlcacher.util.TestConstants;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


public class TermJsonTranslatorTest {

    private JsonReader reader;

    @Test
    public void translateOkJsonObject() throws IOException {
        reader = new JsonReader(new InputStreamReader(new FileInputStream(TestConstants.OK_JSON_FILE_PATH)));
        TermCacheEntry cacheEntry = TermJsonTranslator.translate(reader, TestConstants.BEOGRAD_TERM);
        assertEquals(TestConstants.BEOGRAD_TERM, cacheEntry.getTerm());
        assertEquals(TestConstants.BEOGRAD_FIRST_LINK, cacheEntry.getLink());
    }

    @Test
    public void testInvalidJsonObject() throws IOException {
        reader = new JsonReader(new InputStreamReader(new FileInputStream(TestConstants.INVALID_JSON_FILE_PATH)));
        TermCacheEntry cacheEntry = TermJsonTranslator.translate(reader, TestConstants.BEOGRAD_TERM);
        assertEquals(TestConstants.BEOGRAD_TERM, cacheEntry.getTerm());
        assertNull(cacheEntry.getLink());
    }
}
