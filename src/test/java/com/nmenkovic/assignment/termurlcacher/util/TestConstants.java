package com.nmenkovic.assignment.termurlcacher.util;

import java.io.File;

public class TestConstants {

    public static final String OK_JSON_FILE_PATH = "src" + File.separator + "test" + File.separator + "resources" +
            File.separator + "beograd.json";
    public static final String INVALID_JSON_FILE_PATH = "src" + File.separator + "test" + File.separator +
            "resources" + File.separator + "invalid.json";
    public static final String BEOGRAD_TERM = "beograd";
    public static final String BEOGRAD_FIRST_LINK = "http://dx.doi.org/10.1038/160476a0";
}
